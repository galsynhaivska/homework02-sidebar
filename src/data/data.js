import search from "../icon/search-dark.svg"
import dashboard from "../icon/dashboard-dark.svg";
import revenue from "../icon/revenue-dark.svg";
import notification from "../icon/notification-dark.svg";
import analytic from "../icon/analytic-dark.svg";
import inventory from "../icon/inventory-dark.svg";
import logout from "../icon/logout-dark.svg";
import lightmode from "../icon/lightmode-dark.svg";


const labels = ["Search", "Dashboard", "Revenue", "Notifications",  "Analytic", "Inventory",  "Logout", 
   "Lightmode"];

    const icons = [search, dashboard, revenue, notification, analytic, inventory, logout, lightmode];
    export default icons
  