import React from "react";

import "./item.css"

const Item = ({data, icon}) => {
    
    return(
        <li className="item">
            <div className="item-img"><img src={icon} alt={data}/></div>            
        </li>
    )
}
export default Item