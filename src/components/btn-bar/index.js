import React from "react";

import "./btn-bar.css"

const ButtonBar = ({title}) => {
    return (
        <div className="btn-bar">{title}</div>
    )
}

export default ButtonBar;
