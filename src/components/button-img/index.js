import React from "react";

import "./button-img.css"

function ButtonImg ({src, alt}){
    return (
        <img className="button-img" src={src} alt={alt}></img>
    )
}

export default ButtonImg