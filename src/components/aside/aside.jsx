import React from "react";
import AsideTitle from "../aside-title";
import ItemAside from "../item-aside";
import icons from "../../data/data";
import ButtonBar from "../btn-bar";
import ItemMode from "../item-mode";

import "./aside.css";

const labels = ["Search", "Dashboard", "Revenue", "Notifications",  "Analytic", "Inventory",  "Logout", 
   "Light mode"];
const Aside = () => {
    return(   
        <aside className="aside">
            <ButtonBar title="<"></ButtonBar>
            <div className="items-top">           
               <AsideTitle></AsideTitle>
                <ul className="list-top">
                    <ItemAside data={labels[0]} icon={icons[0]} key={0}></ItemAside>
                    <ItemAside data={labels[1]} icon={icons[1]} key={1}></ItemAside>
                    <ItemAside data={labels[2]} icon={icons[2]} key={2}></ItemAside>
                    <ItemAside data={labels[3]} icon={icons[3]} key={3}></ItemAside>
                    <ItemAside data={labels[4]} icon={icons[4]} key={4}></ItemAside>
                    <ItemAside data={labels[5]} icon={icons[5]} key={5}></ItemAside>
                </ul>
            </div>
            <ul className="list-bottom">
                <ItemAside data={labels[6]} icon={icons[6]} key={6}></ItemAside>
                    <div className="item-last-mode">
                        <ItemAside data={labels[7]} icon={icons[7]} key={7}></ItemAside>
                        <ItemMode></ItemMode>
                    </div> 
            </ul>
        </aside>
    )
}
export default Aside