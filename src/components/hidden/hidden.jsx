import React from "react";
import HiddenTitle from "../hidden-title";
import Item from "../item";
import icons from "../../data/data";
import ButtonBar from "../btn-bar";
import ItemMode from "../item-mode";



import "./hidden.css";


const Hidden = () => {
    return(
        <aside className="hidden">
            <ButtonBar title=">"></ButtonBar>
            <div className="items-top">
               <HiddenTitle></HiddenTitle>
                <ul className="list-top">
                    <Item icon={icons[0]} key={0 + "h"}></Item>
                    <Item icon={icons[1]} key={1 + "h"}></Item>
                    <Item icon={icons[2]} key={2 + "h"}></Item>
                    <Item icon={icons[3]} key={3 + "h"}></Item>
                    <Item icon={icons[4]} key={4 + "h"}></Item>
                    <Item icon={icons[5]} key={5 + "h"}></Item>
                </ul>
            </div>
            <ul className="list-bottom">
                <Item  icon={icons[6]} key={6 + "h"}></Item>   
                <ItemMode></ItemMode>                                    
            </ul>
        </aside>
    )
}
export default Hidden