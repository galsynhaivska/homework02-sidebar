import React from "react"
import Aside from "../aside/aside";
import Hidden from "../hidden/hidden";
import ItemMode from "../item-mode";



import "./app.css"


function App () {
    return (
        <>
            <header className="header">
                <h1 className="h1-title">Navigation</h1>
                <p className="info">Sidebar Navigation</p>
                <div className="line"></div>
            </header>
         
            <main className="container">
                <div className="aside-bar">
                    <Hidden></Hidden>
                    <Aside></Aside>
                </div>
                <section className="main-section">
                    <h1 className="h1-title">Welcome to React</h1>
                </section>
            </main>   
        </>
    )
}

export default App;