import React from "react";
import avatar from "../../icon/avatar-dark.svg";


import "./aside-title.css"

function AsideTitle () {
    return (
        <div className="title-aside">
            <div className="title-img">
                <img className="img-avatar" src={avatar} alt=""/>
            </div>
            <div className="title-info">
                <h2 className="h2-title">Animated Fred</h2>
                <p className="email">animated@demo.com</p>   
            </div>    
           
        </div>
    )

}
export default AsideTitle