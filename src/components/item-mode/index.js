import React from "react";
import ButtonImg from "../button-img";
import moondark from "../../icon/moon-dark.svg";
import moonlight from "../../icon/lightmode-dark.svg";

import "./item-mode.css"

function ItemMode () {
    return (
        <div className="radio-toggle">    
            <input id="light" class="toggle-item item-light" type="radio" name="radio" value="light"></input>
            <label className="label-img1" for="light"><ButtonImg src={moonlight} alt="dark"></ButtonImg></label>
           
            <input id="dark" class="toggle-item item-dark" type="radio" name="radio" value="dark" checked></input>    
            <label className="label-img2" for="dark"><ButtonImg src={moondark} alt="dark"></ButtonImg></label>
        </div> 
    )
}

export default ItemMode;