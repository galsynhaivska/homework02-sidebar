import React from "react";

import "./item-aside.css"

const ItemAside = ({data, icon}) => {
    
    return(
        <li className="item-aside">
            <div className="item-img"><img src={icon} alt={data}/></div>
            <p className="data-item">{data}</p> 
        </li>
    )
}
export default ItemAside